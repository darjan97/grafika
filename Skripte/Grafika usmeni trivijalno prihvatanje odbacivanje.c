typedef unsigned int outcode;
enum { TOP = 0x8, BOTTOM = 0x4, RIGHT = 0x2, LEFT = 0x1 };
void CohenSutherlandLineClipAndDraw( CDC* pDC, double x0, double y0, double x1, double y1, double xmin, double xmax, double ymin, duble ymax, COLORREF value){
    outcode outcode0, outcode1, outcodeOut;
    boolean accept = FALSE, not_done = TRUE;
    outcode0 = CompOutCode( x0, y0, xmin, ymin, xmax, ymax );
    outcode1 = CompOutCode( x1, y1, xmin, ymin, xmax, ymax );

    while (not_done) {
        if( !(outcode0|outcode1) ) { /* Trivijalno prihvatanje */
            accept = TRUE;
            not_done = FALSE;
        }
        else if( outcode0&outcode1) /* Trivijalno odbacivanje */
            not_done = FALSE;
        else {
            double x, y;
            outcodeOut = outcode0 ? outcode0 : outcode1;
            /* Nalazenje presecne tacke */
            if ( outcodeOut & TOP ) {
                x = x0 + (x1-x0)*(ymax - y0)/(y1 - y0);
                y = ymax;
            }
            else if (outcodeOut & BOTTOM) {
                x = x0 + (x1-x0)*(ymin - y0)/(y1 - y0);
                y = ymin;
            }
            else if (outcodeOut & RIGHT) {
                y = y0 + (y1-y0)*(xmax - x0)/(x1 - x0);
                x = xmax;
            }
            else {
                y = y0 + (y1-y0)*(xmin - x0)/(x1 - x0);
                x = xmin;
            }
            if ( outcodeOut == outcode0 ) {
                x0 = x;
                y0 = y;
                outcode0 = CompOutCode( x0, y0, xmin, xmax, ymin, ymax);
            }
            else {
            x1 = x;
            y1 = y;
            outcode1 = CompOutCode( x1, y1, xmin, xmax, ymin, ymax);
            }
        }
    }
    
    if ( accept ) {
        BresenhamLine( pDC, x0, y0, x1, y1, value );/** Crtanje linije **/
    }
}